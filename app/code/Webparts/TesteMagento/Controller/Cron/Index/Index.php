<?php
namespace Webparts\TesteMagento\Controller\Cron\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $productFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Catalog\Model\ProductFactory $productFactory)
	{
		$this->productFactory = $productFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        $product = $product->load($product->getIdBySku('12345'));
        
        if (!$product){
            return $this->createProduct();
        } else (
            return $this->updateProduct($product);
        )
	}

    public function createProduct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('\Magento\Catalog\Model\Product');
        $product->setSku('12345'); 
        $product->setName('produtoteste');
        $product->setAttributeSetId(4);
        $product->setStatus(1); 
        $product->setWeight(10); 
        $product->setVisibility(4);
        $product->setTaxClassId(0);
        $product->setTypeId('simple');
        $product->setPrice(100);
        $product->setStockData(
                                array(
                                    'use_config_manage_stock' => 0,
                                    'manage_stock' => 1,
                                    'is_in_stock' => 1,
                                    'qty' => 10
                                )
                            );
        $product->save();
    }

    public function updateProduct($product)
    {   
        $stockItem = $product->getExtensionAttributes()->getStockItem()>getQty();
        $price = $product->getPrice();
        $newPrice = $price+10;
        $newQty = $stockItem+1;
        $product->setPrice($newPrice);
        $product->setStockData(
            array(
                'qty' => $newQty
                )
            );
        $product->save();       
    }
}
